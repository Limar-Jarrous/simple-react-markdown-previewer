import React from 'react';
import Markdown from 'react-remarkable';
import './App.css';


let defaultText = `# Welcome to my React Markdown Previewer!

## This is a sub-heading...
### And here's some other cool stuff:
  
Heres some code, \`<div></div>\`, between 2 backticks.

\`\`\`
// this is multi-line code:

function anotherExample(firstLine, lastLine) {
  if (firstLine == '\`\`\`' && lastLine == '\`\`\`') {
    return multiLineCode;
  }
}
\`\`\`
  
You can also make text **bold**... whoa!
Or _italic_.
Or... wait for it... **_both!_**
And feel free to go crazy ~~crossing stuff out~~.

There's also [links](https://www.freecodecamp.com), and
> Block Quotes!

And if you want to get really crazy, even tables:

Wild Header | Crazy Header | Another Header?
------------ | ------------- | ------------- 
Your content can | be here, and it | can be here....
And here. | Okay. | I think we get it.

- And of course there are lists.
  - Some are bulleted.
     - With different indentation levels.
        - That look like this.


1. And there are numbererd lists too.
1. Use just 1s if you want! 
1. But the list goes on...
- Even if you use dashes or asterisks.
* And last but not least, let's not forget embedded images:

![React Logo w/ Text](https://picsum.photos/200)
`;



class App extends React.Component{

  state = {
    txt: defaultText,
    maximizeEditor: false,
    maximizePreviewer: false,
  };

  toggleClass = (el, oldClass, newClass) => {
    el.classList.remove(oldClass);
    el.classList.add(newClass);
  };

  handleChange = (e) => {
    this.setState({
      txt: e.target.value,
    });
  };

  handleMaximizeEditor = () => {
    this.setState({
      maximizeEditor: !this.state.maximizeEditor,
    })

    const editView = document.querySelector('.text-editor');
    const expandIcon = document.querySelector('.expand.Text');

    if (expandIcon.children[0].classList.contains('fa-expand')){
      editView.style.width = '100%';
      editView.style.minHeight = '100%';
      document.querySelector('#editor').style.height = '621px';
      this.toggleClass(expandIcon.children[0], 'fa-expand', 'fa-compress');
    }
    else {
      editView.style.width = '80%';
      editView.style.minHeight = '230px';
      document.querySelector('#editor').style.height = '200px';
      this.toggleClass(expandIcon.children[0], 'fa-compress', 'fa-expand');
    }
  };

  handleMaximizePreviewer = () => {
    this.setState({
      maximizePreviewer: !this.state.maximizePreviewer,
    })

    const preView = document.querySelector('.text-previewer');
    const expandIcon = document.querySelector('.expand.Preview');

    if (expandIcon.children[0].classList.contains('fa-expand')){
      preView.style.width = '100%';
      preView.style.minHeight = '100%';
      document.querySelector('#preview').style.height = '621px';
      this.toggleClass(expandIcon.children[0], 'fa-expand', 'fa-compress');
    }
    else {
      preView.style.width = '80%';
      preView.style.minHeight = '230px';
      document.querySelector('#preview').style.height = '200px';
      this.toggleClass(expandIcon.children[0], 'fa-compress', 'fa-expand');
    }
  };


  render(){
    return (
      <div className="body">
        <div className="left">
          <div className="text-editor">
            <header className="editor-header">
              <i className="fa fa-code"></i>
              <span className="title">Editor</span>
              <button className="expand Text"
                      onClick = {this.handleMaximizeEditor}>
                <i className="fa fa-expand"></i>
              </button>
            </header>
            <textarea id="editor"
                      onChange = {this.handleChange}
                      placeholder = {this.state.txt}>
            </textarea>
          </div>
        </div>
        <div className="right">
          <div className="text-previewer">
            <header className="previewer-header">
              <i className="fa fa-code"></i>
              <span className="title">Previewer</span>
              <button className="expand Preview"
                      onClick = {this.handleMaximizePreviewer}>
                <i className="fa fa-expand"></i>
              </button>
            </header>
            <div id="preview">
              <Markdown>
                {this.state.txt}
              </Markdown>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default App;
